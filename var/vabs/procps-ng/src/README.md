# procps-ng build for VectorLinux #

This is a somewhat modified version of the slackware build proceduire.

Slackware builds ``psmisc``, ``procinfo`` and ``procinfo-ng`` into their
procps-ng package.  

On the 7.2 series, VL did not built ``procps-ng`` and ``psmisc`` separately by
themselves.  ``procinfo`` was not built.

On the 8.0 series, VL will feature ``psmisc`` into the ``procps-ng`` package
while maintaining a separate package for ``psmisc``
