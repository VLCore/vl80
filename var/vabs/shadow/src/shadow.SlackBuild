#!/bin/sh

# Copyright 2005-2012  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME=shadow
VERSION=${VERSION:-4.2.1}
LINK=${LINK:-"http://slackware.osuosl.org/slackware-current/source/a/${NAME}/${NAME}-${VERSION}.tar.xz"}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"acl"} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
NUMJOBS=${NUMJOBS:--j6}

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-shadow

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf shadow-$VERSION
tar xvf $CWD/shadow-$VERSION.tar.?z* || exit 1
cd shadow-$VERSION

for patch in $CWD/patches/*; do
  mkdir -p $PKG/usr/doc/${NAME}-${VERSION}
  cp $patch $PKG/usr/doc/${NAME}-${VERSION}/
done

# Relax the restrictions on "su -c" when it is used to become root.
# It's not likely that root is going to try to inject commands back into
# the user's shell to hack it, and the unnecessary restriction is causing
# breakage:
zcat $CWD/patches/shadow.CVE-2005-4890.relax.diff.gz | patch -p1 --verbose || exit 1

chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --mandir=/usr/man \
  --docdir=/usr/doc/shadow-$VERSION \
  --enable-subordinate-ids \
  --disable-shared \
  --without-libcrack \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

#  --enable-utmpx   # defaults to 'no'

make $NUMJOBS || exit 1
make install DESTDIR=$PKG || exit 1

# Fix user group = 100:
zcat $CWD/useradd.gz > $PKG/etc/default/useradd

# /bin/groups is provided by coreutils.
rm -f $PKG/bin/groups
find $PKG -name groups.1 -exec rm {} \;

# Install a login.defs with unsurprising defaults:
rm -f $PKG/etc/login.defs
zcat $CWD/login.defs.gz > $PKG/etc/login.defs.new

mv $PKG/etc/login.access $PKG/etc/login.access.new

# I don't think this works well enough to recommend it.
#mv $PKG/etc/limits $PKG/etc/limits.new
rm -f $PKG/etc/limits

# Add the friendly 'adduser' script:
cat $CWD/adduser > $PKG/usr/sbin/adduser
chmod 0755 $PKG/usr/sbin/adduser

# Add sulogin to the package:
cp -a src/sulogin $PKG/sbin
( cd $PKG/bin ; ln -s ../sbin/sulogin )

# Add the empty faillog log file:
mkdir -p $PKG/var/log
touch $PKG/var/log/faillog.new

# Put some stuff back in "old" locations and make symlinks for compat
( cd $PKG/usr/bin
  mv faillog ../sbin
  mv lastlog ../sbin
  ln -s ../sbin/faillog
  ln -s ../sbin/lastlog
)

# Use 4711 rather than 4755 permissions where setuid root is required:
find $PKG -type f -perm 4755 -exec chmod 4711 "{}" \;

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

mkdir -p $PKG/usr/doc/shadow-$VERSION
cp -a \
  COPYING* NEWS README* TODO doc/{README*,HOWTO,WISHLIST,*.txt} \
  $PKG/usr/doc/shadow-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${NAME}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP

fi

