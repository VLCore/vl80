config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

# Keep same perms on rc.mysqld.new:
if [ -e etc/rc.d/rc.mysqld ]; then
  cp -a etc/rc.d/rc.mysqld etc/rc.d/rc.mysqld.new.incoming
  cat etc/rc.d/rc.mysqld.new > etc/rc.d/rc.mysqld.new.incoming
  mv etc/rc.d/rc.mysqld.new.incoming etc/rc.d/rc.mysqld.new
fi

config etc/rc.d/rc.mysqld.new
config etc/mysqlaccess.conf.new
config etc/my.cnf.new
config etc/my.cnf.d/client.cnf.new
config etc/my.cnf.d/mysql-clients.cnf.new
config etc/my.cnf.d/server.cnf.new
config etc/logrotate.d/mysql.new

getent group mysql >/dev/null || groupadd -g 40 mysql
getent shadow mysql >/dev/null || useradd -c "MariaDB Server" -d /var/lib/mysql -g mysql -s /bin/false -u 40 mysql

mysql_install_db --basedir=/usr --datadir=/var/lib/mysql --user=mysql >/dev/null 2>&1

chown -R mysql:mysql /var/lib/mysql


echo "Read /etc/rc.d/rc.mysqld on how to prepare and start mariadb!!"

